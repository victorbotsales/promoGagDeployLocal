(function () {
    'use strict';
    angular
        .module('myApp')
        .constant("baseURL", "http://localhost:9000/")
        .factory('factItensPromocao', factItensPromocao);

    factItensPromocao.$inject = ['$resource', 'baseURL', '$q'];

    function factItensPromocao($resource, baseURL, $q) {

        var service = {
            getAccessToken: getAccessToken,
            getCategorias: getCategorias,
            getItensPromocao: getItensPromocao,
            getPromocao: getPromocao,
            submeterPromocao: submeterPromocao

        };
        return service;


        function getAccessToken() {
            var deferred = $q.defer();
            FB.getLoginStatus(function(response) {
                console.log(response);
                if (response.status === 'connected') {
                    deferred.resolve(response.authResponse.accessToken);
                } else {
                    // TODO chamar login novamente
                    deferred.reject('Error occured');
                }
            });

            return deferred.promise;
        }


        function getCategorias() {
            return $resource(baseURL + "categorias/:id", null, {
                'update': {
                    method: 'GET'
                }
            });
            //return categorias;
        }

        function getItensPromocao() {
            return $resource(baseURL + "itensPromocao/:id", null, {
                'update': {
                    method: 'PUT'
                }
                
            });
            //return itensPromocao;
        }

        function getPromocao(id) {
            //console.log(baseURL + "itensPromocao/" + id);
            return $resource(baseURL + "itensPromocao/" + id, null, {
                'query': {
                    method: 'GET'
                }

            });
            //return itensPromocao;
        }

        function submeterPromocao() {
            return $resource(baseURL + "itensPromocao/", null, {
                'add': {
                    method: 'POST'
                }
            });
        }
    }








})();

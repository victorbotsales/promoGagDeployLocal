(function () {
    'use strict';
    angular
        .module('myApp')
        .controller('LoginController', LoginController);


    // O $scope foi utilizado para conseguir estabelecer um controle da variável usuariologado dentro da função do FB.login, pois sem o uso do $scope.apply não funciona!
    LoginController.$inject = ['$scope', 'factItensPromocao'];

    function LoginController($scope, factItensPromocao) {
        var vm = this;

        vm.login = login;
        vm.logout = logout;
        vm.usuariologado = false;

        factItensPromocao.getAccessToken().then(function (acessToken) {
            vm.usuariologado = true;
        })

        function login() {
            FB.login(function (response) {
                if (response.status === 'connected') {
                    // Logged into your app and Facebook.
                    console.log(response);
                    $scope.$apply(function () {
                        vm.usuariologado = true;
                    });
                    swal("Logado com sucesso!", "", "success");
                    $('#modal1').closeModal();


                } else if (response.status === 'not_authorized') {
                    sweetAlert("Oops...", "Ocorreu um erro, tente novamente!", "error");
                    console.log("Favor autorizar o aplicativo.");

                } else {
                    sweetAlert("Oops...", "Ocorreu um erro, tente novamente!", "error");
                    console.log("Favor autorizar o aplicativo.");
                }
            }, {
                scope: 'public_profile,email'
            });
        }

        function logout() {
            FB.logout(function (response) {
                $scope.$apply(function () {
                    vm.usuariologado = false;
                })
                swal("Deslogado com sucesso!", "", "success");
                // Evitar o bug do overlay!!!
                $('.lean-overlay').closeModal()
            });
        }
    }
})();
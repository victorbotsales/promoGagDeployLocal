(function () {
    'use strict';
    angular
        .module('myApp')
        .controller('MainController', MainController);

    MainController.$inject = ['factItensPromocao'];

    function MainController(factItensPromocao) {

        // vm = representa o viewModel do MainPage ( John Papa Angular (controllerAs with vm) //
        var vm = this;

        /*
         Array que armazena todos os objetos itensPromocao da chamada do getItensPromocao
         */

        vm.categorias = ["Celulares e Telefones", "Informática", "Tv e Eletrônicos", "Eletrodomésticos", "Moda e Acessórios", "Móveis e Decoração", "Esporte e Lazer"];

        vm.categoriaSelecionada = "";

        vm.fecharSideNav = fecharSideNav;

        vm.itensPromocao = [];

        vm.minusOne = minusOne;

        vm.plusOne = plusOne;

        vm.selecionarCategoria = selecionarCategoria;

        vm.sort = '-(likes-dislikes)';

        /*
         Ativação dos Componentes!
         */
        activateComponents();

        /*
         Função para ativar os componentes do Modal (Utilizado pelo login) e também pela navegação lateral (sideNav) utilizado no mobile!
         */
        function activateComponents() {
            $(document).ready(function () {
                // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
                $('.modal-trigger').leanModal();
                $('.button-collapse').sideNav();
            });
        }

        /*
         Função que verifica se a tecla "enter" foi pressionada, e se sim, irá ocultar a navegação lateral.
         */
        function fecharSideNav(keyEvent) {
            if (keyEvent.which == 13) {
                $('.button-collapse').sideNav('hide');
            }

        }

        factItensPromocao.getItensPromocao().query(
            function (response) {
                vm.itensPromocao = response;

            },
            function (response) {
                vm.message = "Error: " + response.status + " " + response.statusText;
            })


        /*
         Função para aumentar o número de dislikes a cada clique executado no botão "Não Curti".
         obs
         */
        function minusOne(itemPromocao) {
            itemPromocao.dislikes++;
            factItensPromocao.getItensPromocao().update({id: itemPromocao.id}, itemPromocao, null, function () {
                itemPromocao.dislikes--;
                sweetAlert("Oops...", "Você ja votou!", "error");
            })
        }

        /*
         Função para aumentar o número de likes a cada clique executado no botão "Curti"
         */
        function plusOne(itemPromocao) {
            itemPromocao.likes++;
            factItensPromocao.getItensPromocao().update({id: itemPromocao.id}, itemPromocao, null, function () {
                itemPromocao.likes--;
                sweetAlert("Oops...", "Você ja votou!", "error");
            })

        }


        /*
         Função para Selecionar a categoria dependendo de que botão está sendo clicado, modificando o valor da
         string (categoriaSelecionada) para realizar o filtro como desejado.
         */

        function selecionarCategoria(indexCategoria) {
            if (indexCategoria == -1) {
                vm.categoriaSelecionada = "";
            } else {
                vm.categoriaSelecionada = vm.categorias[indexCategoria];
            }
        }


    }

})();

(function () {
    'use strict';
    angular
        .module('myApp')
        .controller('PromocaoDetailController', PromocaoDetailController)

    PromocaoDetailController.$inject = ['factItensPromocao', '$routeParams'];

    function PromocaoDetailController(factItensPromocao, $routeParams) {


        // vm = representa o viewModel do MainPage ( John Papa Angular (controllerAs with vm) //
        var vm = this;

        vm.promocao = "";

        setTimeout(activateComponents, 1);
        /*
         Função para ativar os componentes do Modal (Utilizado pelo login) e também pela navegação lateral (sideNav) utilizado no mobile!
         */
        function activateComponents() {
            $(document).ready(function () {
                // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
                $('.modal-trigger').leanModal();
                $('.button-collapse').sideNav();
            });
        }

        factItensPromocao.getPromocao($routeParams.id).query(
            function (response) {
                console.log(response);
                vm.promocao = response;
            },
            function (response) {
                vm.message = "Error: " + response.status + " " + response.statusText;
            })

    }

})();

'use strict';

angular
    .module('myApp')
    .config(config);

config.$inject = ['$routeProvider'];

function config($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'app/views/main.html',
                controller: 'MainController',
                controllerAs: 'mainVm'
            })
            .when('/addPromocao', {
                templateUrl: 'app/views/form.html',
                controller: 'FormController',
                controllerAs: 'formVm'
            })
            .when('/promocaoDetail/:id', {
                url: 'promocao/:id',
                templateUrl: 'app/views/promocaoDetail.html',
                controller: 'PromocaoDetailController',
                controllerAs: 'detailVm',
            })
             .otherwise({
                redirectTo:'/'
            });
    }

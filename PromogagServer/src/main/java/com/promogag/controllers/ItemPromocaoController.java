package com.promogag.controllers;

import com.promogag.domain.ItemPromocao;
import com.promogag.domain.Votacao;
import com.promogag.repository.ItemPromocaoRepository;
import com.promogag.repository.VotacaoRepository;
import com.promogag.services.ItemPromocaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Francisco on 22/06/2016.
 */
@RestController
@RequestMapping("itensPromocao")
public class ItemPromocaoController {

    private ItemPromocaoService itemPromocaoService;

    @Autowired
    private ItemPromocaoRepository itemPromocaoRepository;

    @Autowired
    private VotacaoRepository votacaoRepository;


    @Autowired
    public ItemPromocaoController(ItemPromocaoService itemPromocaoService) {
        this.itemPromocaoService = itemPromocaoService;
    }

    @RequestMapping( value = "", method = RequestMethod.GET)
    public Iterable<ItemPromocao> list(){return itemPromocaoService.list();}

    @RequestMapping( value = "", method = RequestMethod.POST )
    public ItemPromocao create(@RequestBody ItemPromocao itemPromocao, @RequestParam(value = "accessToken") String accessToken){
        System.out.println(accessToken);

        //usar spring social para obter dados do usuario
        Facebook facebook = new FacebookTemplate(accessToken);

        String urlProfile = "http://graph.facebook.com/" + facebook.userOperations().getUserProfile().getId() + "/picture?type=square";
        String idUser = facebook.userOperations().getUserProfile().getId();
        String nameUser = facebook.userOperations().getUserProfile().getName();

        itemPromocao.setNameUser(nameUser);
        itemPromocao.setIdUser(idUser);
        itemPromocao.setProfilePicture(urlProfile);

        //adicionar novos campos de id user, name user e profile url no itempromocao
        return itemPromocaoService.create(itemPromocao);
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.GET )
    public ItemPromocao read(@PathVariable(value = "id")long id){return itemPromocaoService.read(id);}


    @RequestMapping( value = "/{id}", method = RequestMethod.PUT)
    public ItemPromocao update(@PathVariable(value = "id")long id, @RequestBody ItemPromocao itemPromocao, HttpServletRequest request) {

        //FIXME extrair para votacao controller
        Votacao auxVotacao = new Votacao();
        String ipUsuario = request.getRemoteAddr();
        if(votacaoRepository.findByipAddressAndItemVotado(ipUsuario,id).size() == 0) {
            auxVotacao.setIpAddress(ipUsuario);
            auxVotacao.setItemVotado(id);
            votacaoRepository.save(auxVotacao);
            return itemPromocaoService.upgrade(id, itemPromocao);
        } else {
            throw new RuntimeException("teste");
        }
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id")long id){itemPromocaoService.delete(id);}

}

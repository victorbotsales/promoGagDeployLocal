package com.promogag.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by bot on 6/26/2016.
 */
@Entity
@Table(name = "votacao")
public class Votacao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NotNull
    private String ipAddress;
    @NotNull
    private long itemVotado;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public long getItemVotado() {
        return itemVotado;
    }

    public void setItemVotado(long itemVotado) {
        this.itemVotado = itemVotado;
    }
}

package com.promogag.repository;

import com.promogag.domain.ItemPromocao;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Francisco on 22/06/2016.
 */
public interface ItemPromocaoRepository extends CrudRepository<ItemPromocao, Long> {
}

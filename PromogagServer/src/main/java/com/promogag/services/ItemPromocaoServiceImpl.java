package com.promogag.services;

import com.promogag.domain.ItemPromocao;
import com.promogag.repository.ItemPromocaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Francisco on 22/06/2016.
 */
@Service
@Transactional
public class ItemPromocaoServiceImpl implements ItemPromocaoService {

    private ItemPromocaoRepository itemPromocaoRepository;


    @Autowired
    public ItemPromocaoServiceImpl(ItemPromocaoRepository itemPromocaoRepository) {
        this.itemPromocaoRepository = itemPromocaoRepository;
    }

    @Override
    public Iterable<ItemPromocao> list() {
        return itemPromocaoRepository.findAll();
    }

    @Override
    public ItemPromocao create(ItemPromocao itemPromocao) {
        return itemPromocaoRepository.save(itemPromocao);
    }

    @Override
    public ItemPromocao read(long id) {
        return itemPromocaoRepository.findOne(id);
    }

    @Override
    public ItemPromocao upgrade(long id, ItemPromocao itemPromocao) {
        ItemPromocao auxItem = itemPromocaoRepository.findOne(id);
        if (auxItem.getTitulo() != null) {
            long auxId = auxItem.getId();
            auxItem = itemPromocao;
            auxItem.setId(auxId);
            return itemPromocaoRepository.save(auxItem);
        }
        return null;
    }


    @Override
    public void delete(long id) {
        itemPromocaoRepository.delete(id);
    }
}

package com.promogag.services;

import com.promogag.domain.ItemPromocao;

/**
 * Created by Francisco on 22/06/2016.
 */
public interface ItemPromocaoService {
    Iterable<ItemPromocao> list();

    ItemPromocao create(ItemPromocao itemPromocao);

    ItemPromocao read(long id);

    ItemPromocao upgrade(long id, ItemPromocao itemPromocao);
    
    void delete(long id);
}

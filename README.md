Promogag
--------

O projeto consiste em um sistema de publicação de promoções oferecidas por sites de vendas online e que permite a avaliação destas por parte dos usuários. Cada publicação representará um elemento independente no sistema, dotado de uma pontuação baseada na interação dos usuários com a publicação. A pontuação de cada publicação será utilizada para medir a inclinação dos usuários em relação a promoção e ordenar os elementos nas páginas de exibição de anúncios do sistema.

Usage
-----

 **1.** Baixe e instale o xampp:      
        
        https://www.apachefriends.org/pt_br/download.html

**2.** Baixe o Intelij versão ultimate (estudantes da UFRPE têm acesso grátis através do e-mail do zimbra)

        https://www.jetbrains.com/idea/

**3.** Após abrir o intelij, clique em file->open e abra o diretório que foi clonado do git


**4.** Após importar o PromogagServer, acesse a aba gradle, na extrema direita do intelij e clique em refresh

**5.** Agora abra o xampp, e inicie os servidores apache e mysql


**6.** Após iniciar os servidores no xampp, retorne ao intelij, navegue até src->main->java, clique com o botão direito no mouse em PromogagAplication, e clique em run PromogagAplication
        
**7.** Agora, navegue pelo intelij para a pasta src->webapp, e execute o index.html
        